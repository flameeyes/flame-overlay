;; js2-mode site-lisp configuration

(add-to-list 'load-path "@SITELISP@")
(autoload 'yaml-mode "yaml-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
