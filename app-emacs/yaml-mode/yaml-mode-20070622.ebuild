# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit elisp

DESCRIPTION="A major mode for GNU emacs for editing YAML files."
HOMEPAGE="http://svn.clouder.jp/repos/public/yaml-mode/trunk/"
SRC_URI="http://www.flameeyes.eu/gentoo-distfiles/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND=""
DEPEND=""

SITEFILE=50${PN}-gentoo.el

src_compile() {
	elisp-compile ${PN}.el || die "bytecompile failed"
}

src_install() {
	elisp-install ${PN} ${PN}.el || die "elisp-install failed"
	elisp-site-file-install "${FILESDIR}/${SITEFILE}" || die "elisp-site-file-install failed"

	dodoc README Changes
}
