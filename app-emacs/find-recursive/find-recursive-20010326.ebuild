# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

SIMPLE_ELISP=t

inherit elisp

DESCRIPTION="Find files recursively into a directory"
HOMEPAGE="http://www.webweavertech.com/ovidiu/emacs/index.html"
SRC_URI="http://www.flameeyes.eu/gentoo-distfiles/${P}.el.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE=""

SITEFILE=50${PN}-gentoo.el
