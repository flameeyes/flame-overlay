# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

SIMPLE_ELISP=t

inherit elisp

DESCRIPTION="Insert snippets of text into a buffer"
HOMEPAGE="http://www.kazmier.com/computer/"
SRC_URI="http://www.flameeyes.eu/gentoo-distfiles/${P}.el.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE=""

SITEFILE=50${PN}-gentoo.el
