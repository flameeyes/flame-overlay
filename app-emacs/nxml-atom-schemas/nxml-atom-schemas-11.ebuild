# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-emacs/nxml-svg-schemas/nxml-svg-schemas-1.1.20081123.ebuild,v 1.2 2008/11/26 10:02:11 armin76 Exp $

inherit elisp

DESCRIPTION="Extension for nxml-mode with Atom 1.0 schemas"
HOMEPAGE="http://www.atompub.org/"

SRC_URI="http://www.flameeyes.eu/gentoo-distfiles/atom-${PV}.rnc.bz2"

LICENSE="as-is"

SLOT="0"

KEYWORDS="~amd64"
IUSE=""

RDEPEND="|| ( >=app-emacs/nxml-mode-20041004-r3 >=virtual/emacs-23 )"

SITEFILE=60${PN}-gentoo.el

S="${WORKDIR}"

src_compile() { :; }

src_install() {
	insinto ${SITEETC}/${PN}
	doins "${FILESDIR}/schemas.xml" || die "install failed"
	newins "${WORKDIR}"/atom-11.rnc atom.rnc || die "rnc install failed"
	elisp-site-file-install "${FILESDIR}/${SITEFILE}" || die
}

pkg_postinst () {
	elisp-site-regen

	if [ $(emacs -batch -q --eval "(princ (fboundp 'nxml-mode))") = nil ]; then
		ewarn "This package needs nxml-mode. You should either install"
		ewarn "app-emacs/nxml-mode, or use \"eselect emacs\" to select"
		ewarn "an Emacs version >= 23."
	fi
}
