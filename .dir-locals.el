(
 (sh-mode . (
             (indent-tabs-mode . t)
             (sh-basic-offset . 4)
             (tab-width . 4)
             ))
 )
