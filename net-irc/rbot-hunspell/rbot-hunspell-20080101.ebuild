# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-irc/rbot/rbot-9999-r7.ebuild,v 1.1 2007/12/27 13:46:04 flameeyes Exp $

DESCRIPTION="Hunspell plugin for rbot"

HOMEPAGE="http://www.flameeyes.eu/projects#rbot-hunspell"
SRC_URI="http://www.flameeyes.eu/files/${P}.tar.bz2"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND=">net-irc/rbot-0.9.10
	dev-ruby/ruby-hunspell"

src_install() {
	cd "${S}"

	insinto /usr/share/rbot/plugins
	doins hunspell.rb

	dodoc README
}
