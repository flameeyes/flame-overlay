# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-dotnet/mono-addins/mono-addins-0.3.1.ebuild,v 1.3 2008/07/30 23:21:56 ranger Exp $

EAPI=2

inherit mono

DESCRIPTION="A set of .Net controls for sending and receiving XMPP messages"
HOMEPAGE="http://code.google.com/p/jabber-net/"
SRC_URI="http://jabber-net.googlecode.com/files/${PN}-Source-${PV}.zip"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND=">=dev-lang/mono-1.9
	dev-dotnet/zlib-net"
DEPEND="${RDEPEND}"

src_prepare() {
	# These include a binary version of zlib.net
	rm lib20/* lib11/* || die
}

src_compile() {
	emake dll || die "emake failed"
}

src_install() {
	egacinstall bin/debug/jabber-net.dll || die
	dodoc ChangeLog || die
}
