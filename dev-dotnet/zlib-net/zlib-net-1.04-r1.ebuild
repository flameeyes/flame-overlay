# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-dotnet/mono-addins/mono-addins-0.3.1.ebuild,v 1.3 2008/07/30 23:21:56 ranger Exp $

EAPI=2

inherit mono versionator

DESCRIPTION="A managed rewrite of zlib for .NET"
HOMEPAGE="http://www.componentace.com/products/product.php?id=11"
SRC_URI="http://www.componentace.com/data/distr/zlib.NET_$(delete_all_version_separators).zip"

LICENSE="as-is"
SLOT="0"
KEYWORDS="~amd64"
IUSE="debug"

RDEPEND=">=dev-lang/mono-1.9"
DEPEND="${RDEPEND}"

S=${WORKDIR}

doecho() {
	echo "$@"
	"$@"
}

src_compile() {
	local gmcsflags="-target:library -out:zlib.net.dll -keyfile:${FILESDIR}/mono.snk"

	if use debug; then
		gmcsflags="${gmcsflags} -debug+ -define:'DEBUG,TRACE' -optimize-"
	else
		gmcsflags="${gmcflags} -debug- -optimize+"
	fi

	pushd source
	doecho gmcs ${gmcsflags} *.cs || die
	popd
}

src_install() {
	egacinstall source/zlib.net.dll 2.0 || die
	dodoc readme.txt history.txt || die
}
