# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit mono

DESCRIPTION="A .Net library to handle vCard objects"
HOMEPAGE="http://www.thoughtproject.com/Libraries/vCard/"
SRC_URI="http://www.thoughtproject.com/Libraries/vCard/Files/${P}.zip"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64"
IUSE="debug test"

RDEPEND=">=dev-lang/mono-2.4"
DEPEND="${RDEPEND}
	dev-util/monodevelop"

S="${WORKDIR}"

MY_PN=${PN//-/.}

src_prepare() {
	sed -i \
		-e 's:\r$::' \
		-e '$a [assembly: AssemblyKeyFile("'"${FILESDIR}/mono.snk"'")]' \
		"${S}"/Solution/${MY_PN}/Properties/AssemblyInfo.cs || die
}

src_compile() {
	local config
	use debug && config=Debug || config=Release

	elog "Please ignore pkg-config errors related to blas or libgcj"

	pushd Solution
	mdtool build -c:${config} vCards.sln || die
	popd

	if use test; then
		pushd "Unit Tests"
		mdtool build -c:Debug 'vCards Unit Tests.sln' || die
		popd
	fi
}

src_test() {
	nunit-console2 "Unit Tests/Thought.Net.vCards.UnitTests/bin/Debug/Thought.Net.vCards.UnitTests.dll" \
		|| die "nunit tests failed"
}

src_install() {
	local config
	use debug && config=Debug || config=Release

	egacinstall Solution/${MY_PN}/bin/${config}/${MY_PN}.dll ${MY_PN} || die
	dodoc ReadMe.txt Roadmap.txt Solution/${MY_PN}/History.txt || die
}
