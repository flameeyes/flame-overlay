# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

USE_RUBY="ruby18 ree18"

inherit ruby-ng toolchain-funcs

DESCRIPTION="Ruby bindings for Hunspell spell-checking library"
HOMEPAGE="http://www.flameeyes.eu/projects#ruby-hunspell"
SRC_URI="mirror://rubyforge/${PN}/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86-fbsd"
IUSE=""

RDEPEND="${RDEPEND} >=app-text/hunspell-1.1.9"

ruby_add_bdepend dev-ruby/rust

each_ruby_compile() {
	tc-export CXX

	emake RUBY="${RUBY}" || die "emake failed"
}

each_ruby_install() {
	exeinto "$(ruby_rbconfig_value 'sitearchdir')"
	doexe hunspell/hunspell.so || die
}

all_ruby_install() {
	dodoc GIT-BRANCHES AUTHORS README README.charset || die
}
