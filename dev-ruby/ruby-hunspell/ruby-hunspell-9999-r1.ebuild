# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

USE_RUBY="ruby18 ree18"

inherit ruby-ng toolchain-funcs

if [[ "${PV}" == "9999" ]]; then
	inherit git

	EGIT_REPO_URI="git://git.flameeyes.eu/ruby-hunspell.git"
	SRC_URI=""
fi

DESCRIPTION="Ruby bindings for Hunspell spell-checking library"
HOMEPAGE="http://www.flameeyes.eu/projects#ruby-hunspell"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="${RDEPEND} >=app-text/hunspell-1.1.9"

ruby_add_bdepend dev-ruby/rust

each_ruby_compile() {
	tc-export CXX

	emake RUBY="${RUBY}" || die "emake failed"
}

each_ruby_install() {
	exeinto "$(ruby_rbconfig_value 'sitearchdir')"
	doexe hunspell/hunspell.so || die
}

all_ruby_install() {
	dodoc GIT-BRANCHES AUTHORS README README.charset || die
}
