# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-ruby/ruby-liquid/ruby-liquid-1.0.ebuild,v 1.1 2006/06/24 23:41:43 flameeyes Exp $

EAPI=2

USE_RUBY="ruby18 ruby19 ree18 jruby"
SRC_URI="mirror://rubyforge/${PN}/${P}.tar.bz2"
KEYWORDS="~amd64"

if [[ ${PV} == "9999" ]]; then
	inherit git

	EGIT_REPO_URI="git://gitorious.org/ruby-elf/ruby-elf.git"
	SRC_URI=""
	KEYWORDS=""
fi

inherit ruby-ng

DESCRIPTION="Ruby library to access ELF files information"
HOMEPAGE="http://www.flameeyes.eu/projects/ruby-elf"

LICENSE="GPL-2"
SLOT="0"
IUSE="test"

ruby_add_bdepend "
	test? (
		dev-ruby/rake
		dev-ruby/rubygems
		|| ( virtual/ruby-test-unit dev-ruby/test-unit:2 )
	)"

RDEPEND="${RDEPEND}
	virtual/man"

# the published released tarballs don't require rebuilding the the man
# pages so keep all this thing conditional.
if [[ ${PV} == "9999" ]]; then
	ruby_add_bdepend "dev-ruby/rake dev-ruby/rubygems"

	DEPEND="${DEPEND}
		dev-libs/libxslt
		app-text/docbook-xsl-ns-stylesheets"

	all_ruby_unpack() {
		git_src_unpack
	}

	all_ruby_compile() {
		# build the man pages
		rake manpages || die "emake manpages failed"
	}
fi

each_ruby_install() {
	doruby -r lib/* || die
}

each_ruby_test() {
	${RUBY} -S rake test || die "${RUBY} test failed"
}

all_ruby_install() {
	dobin bin/* || die
	doman manpages/*.1 || die "doman failed"
	dodoc DONATING || die "dodoc failed"
}
