# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-ruby/amazon-ec2/amazon-ec2-0.9.15.ebuild,v 1.1 2010/06/10 22:41:41 flameeyes Exp $

EAPI=2

USE_RUBY="ruby18"

RUBY_FAKEGEM_TASK_DOC="rdoc"
RUBY_FAKEGEM_TASK_TEST="test"

RUBY_FAKEGEM_DOCDIR="doc"
RUBY_FAKEGEM_EXTRADOC="README.rdoc"

inherit ruby-fakegem

DESCRIPTION="make chromium extension"
HOMEPAGE="http://github.com/Constellation/crxmake"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

ruby_add_rdepend '>=dev-ruby/zipruby-0.3.2'

RDEPEND="${RDEPEND}
	ruby_targets_ruby18? ( dev-lang/ruby:1.8[ssl] )"

DEPEND="${DEPEND}
	test? ( ruby_targets_ruby18? ( dev-lang/ruby:1.8[ssl] ) )"

each_ruby_test() {
	# it needs the rubylib prefixed to test the local code rather than
	# the system code.
	RUBYLIB="$(pwd)/lib${RUBYLIB+:${RUBYLIB}}" \
		${RUBY} -S rake test || die "rake test failed"
}
