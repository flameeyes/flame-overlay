# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-ruby/amazon-ec2/amazon-ec2-0.9.15.ebuild,v 1.1 2010/06/10 22:41:41 flameeyes Exp $

EAPI=2

# jruby → native extension
# ruby19 → scary warnings related to RString structures printed directly.
USE_RUBY="ruby18"

RUBY_FAKEGEM_TASK_DOC=""
RUBY_FAKEGEM_TASK_TEST=""

RUBY_FAKEGEM_DOCDIR="doc"
RUBY_FAKEGEM_EXTRADOC="ChangeLog"

# no test present :(
RESTRICT=test

inherit ruby-fakegem

DESCRIPTION="Ruby bindings for libzip"
HOMEPAGE="http://zipruby.rubyforge.org/"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="${RDEPEND}
	sys-libs/zlib"

DEPEND="${DEPEND}
	sys-libs/zlib"

each_ruby_configure() {
	${RUBY} -Cext extconf.rb || die "extconf failed"
}

each_ruby_compile() {
	emake -Cext CFLAGS="${CFLAGS} -fPIC" archflag="${LDFLAGS}" || die "emake failed"
	mkdir lib
	cp -l ext/${PN}.so lib/
}

all_ruby_compile() {
	${RUBY} rdoc || die "rdoc failed"
}
