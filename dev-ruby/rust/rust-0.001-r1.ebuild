# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-ruby/ruby-liquid/ruby-liquid-1.0.ebuild,v 1.1 2006/06/24 23:41:43 flameeyes Exp $

EAPI=2

USE_RUBY="ruby18 ree18"

inherit ruby-ng

DESCRIPTION="Ruby Extensions Generator"
HOMEPAGE="http://www.flameeyes.eu/projects#rust"
SRC_URI="mirror://rubyforge/${PN}/${P}.tar.bz2"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="test doc"

ruby_add_bdepend test "=${CATEGORY}/${PF} virtual/ruby-test-unit"

all_ruby_prepare() {
	sed -i \
		-e 's:@tpls_dir =.*:@tpls_dir = Pathname("/usr/share/rust/templates"):' \
		"${S}"/rust.rb || die
	mv "${S}"/rust/templates "${S}" || die
}

all_ruby_compile() {
	if use doc; then
		emake doc || die "emake doc failed"
	fi
}

each_ruby_test() {
	cd "${S}/test"

	TMPDIR=${T} ${RUBY} -I"${S}" testsuite.rb || die "tests failed"
}

each_ruby_install() {
	doruby -r rust.rb rust || die
}

all_ruby_install() {
	insinto /usr/share/rust/templates
	doins templates/* || die

	insinto /usr/include
	doins include/rust_conversions.hh || die

	if use doc; then
		dohtml -r rust-doc/* || die
	fi

	dodoc AUTHORS README || die
}
