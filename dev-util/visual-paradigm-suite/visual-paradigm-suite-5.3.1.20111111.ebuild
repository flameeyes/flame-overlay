# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit versionator eutils

MY_PN="VP_Suite"
if [[ $(get_version_component_count) == 3 ]]; then
	MY_PV="$(replace_all_version_separators _)"
else
	MY_PV="$(replace_all_version_separators _ $(get_version_component_range 1-2))"
	MY_PV="${MY_PV}_sp$(get_version_component_range 3)"
	MY_PV="${MY_PV}_$(get_version_component_range 4)"
fi

DESCRIPTION="Visual Paradigm Suite"
HOMEPAGE="http://www.visual-paradigm.com"
SRC_URI="${MY_PN}_Linux_NoInstall_${MY_PV}.tar.gz"

S="${WORKDIR}/${MY_PN}$(get_version_component_range 1-2)"

LICENSE="as-is" # actually, proprietary
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RESTRICT="fetch"

DEPEND=""
RDEPEND=">=virtual/jre-1.5
	x11-misc/xdg-utils"

pkg_nofetch() {
	elog "Please download ${SRC_URI} from your Customer Service Center"
	elog "and copy it into ${DISTDIR}"
}

src_install() {
	insinto /opt/Visual-Paradigm/${MY_PN}
	doins -r bin bundled dbva integration launcher lib ormlib \
		resources scripts sde shapes updatesynchronizer \
		UserLanguage .install4j

	rm "${D}"/opt/Visual-Paradigm/${MY_PN}/.install4j/firstrun

	chmod +x \
		"${D}"/opt/Visual-Paradigm/${MY_PN}/bin/VP_Suite \
		"${D}"/opt/Visual-Paradigm/${MY_PN}/launcher/*

	dodoc -r Samples

	make_desktop_entry /opt/Visual-Paradigm/${MY_PN}/launcher/VP_Suite_Product_Launcher "Visual Paradigm Suite Launcher" /opt/Visual-Paradigm/VP_Suite/resources/vpuml.png
}
