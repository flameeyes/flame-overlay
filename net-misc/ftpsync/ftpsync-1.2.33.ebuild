# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="A Perl script to synchronize a local directory tree and a remote FTP directory tree."
HOMEPAGE="http://ftpsync.sourceforge.net/"
SRC_URI="http://ftpsync.sourceforge.net/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="dev-lang/perl
	dev-perl/libwww-perl"

src_install() {
	dodoc TODO README Changes
	dobin ftpsync.pl
}
