# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit distutils eutils

MY_P="${P/_p/-p}"
S="${WORKDIR}/${MY_P}"

DESCRIPTION="Unobstrudive notifier"
HOMEPAGE="http://code.google.com/p/specto/"
SRC_URI="http://specto.googlecode.com/files/${P}.tar.gz"

LICENSE="GPL-2"

KEYWORDS="~amd64"

SLOT="0"

IUSE=""

RDEPEND="dev-python/gnome-python
	dev-python/notify-python"

DEPEND="${RDEPEND}"

src_unpack() {
	distutils_src_unpack

	# Replace the documentation directory so that it installs in the
	# Gentoo-defined path, rather than its own.
	sed -i -e "s:share/doc/specto:share/doc/${PF}:" \
		"${S}"/setup.py "${S}"/spectlib/util.py || die

	epatch "${FILESDIR}"/136_135.diff
}
