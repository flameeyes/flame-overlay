# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-crypt/ekeyd/ekeyd-1.1.3-r2.ebuild,v 1.2 2011/03/27 22:13:47 flameeyes Exp $

EAPI=4

inherit eutils flag-o-matic toolchain-funcs

DESCRIPTION="Pseudo Random Number Generator Daemon"
HOMEPAGE="http://prngd.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="as-is MIT" # audit pending
SLOT="0"

KEYWORDS="~amd64"

IUSE=""

RDEPEND=""
DEPEND="${RDEPEND}
	dev-lang/perl"

src_prepare() {
	epatch "${FILESDIR}"/${P}-format.patch
}

src_compile() {
	if use kernel_linux; then
		append-cppflags -DLINUX2
	else
		eerror "${PF}.ebuild lacks support for your system." && die
	fi

	emake \
		CC="$(tc-getCC)" \
		CFLAGS="${CFLAGS}" \
		CPPFLAGS="${CPPFLAGS}" \
		prngd prngd.man tools/prngd-ctl || die
}

src_install() {
	exeinto /usr/libexec
	doexe prngd

	dosbin tools/prngd-ctl

	newman prngd.man prngd.1

	newinitd "${FILESDIR}"/${PN}.init ${PN}
	newconfd "${FILESDIR}"/${PN}.conf ${PN}

	insinto /etc
	if use kernel_linux; then
		newins "${S}"/contrib/Linux-2/prngd.conf.linux-2 prngd.conf
	else
		eerror "${PF}.ebuild lacks support for your system." && die
	fi

	dodoc 00*
}
